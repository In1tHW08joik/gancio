---
layout: default
title: Custom CSS
permalink: /usage/custom_css
nav_order: 3
parent: Usage
---

## Custom CSS <span class='label label-yellow'>Since 1.19.0</span> <span class='label label-red'>BETA</span>

Is it possible to modify the style by integrating some custom css.

Don't imagine you can accomplish miracles because the templates are not designed to be easily modified,
but don't be afraid to [open an issue](https://framagit.org/les/gancio/-/issues) or even better a PR to add some css selectors or some usage examples to this page.


### Remove navbar
```css
#navbar {
    display: none;
}
```

> info "References"
> [#413](https://framagit.org/les/gancio/-/issues/413)
